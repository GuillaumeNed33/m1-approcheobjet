package fr.ubordeaux.ao.domain.model;

import fr.ubordeaux.ao.domain.exception.QuestionExamException;

import java.util.ArrayList;
import java.util.Objects;
import java.util.List;

public class Question {
    private String question;
    private String description;
    private List<String> candidateAnswerList;
    private List<String> trueAnswerList;

    public Question(String question, String description, List<String> candidateAnswerList, List<String> trueAnswerList) {
        this.listQuestion(question);
        this.listDescription(description);
        this.listCandidateAnswerList(candidateAnswerList);
        this.listTrueAnswerList(trueAnswerList);
    }

    public String getQuestion() {
        String q_copy = this.question;
        return q_copy;
    }

    public String getDescription() {
        String d_copy = this.description;
        return d_copy;
    }

    public List<String> getCandidateAnswerList() {
        return new ArrayList<String>(this.candidateAnswerList);
    }

    public List<String> getTrueAnswerList() {
        return new ArrayList<String>(this.trueAnswerList);
    }

    private void listQuestion(String question) {
        if (question == null) throw new QuestionExamException("cannot create question with null question");
        this.question = question;
    }

    private void listDescription(String description) {
        if (description == null) throw new QuestionExamException("cannot create question with null description");
        this.description = description;
    }

    private void listCandidateAnswerList(List<String> candidateAnswerList) {
        if (candidateAnswerList == null) throw new QuestionExamException("cannot create question with null candidateAnswerList");
        this.candidateAnswerList = candidateAnswerList;
    }

    private void listTrueAnswerList(List<String> trueAnswerList) {
        if (trueAnswerList == null) throw new QuestionExamException("cannot create question with null trueAnswerList");
        for(String answer : trueAnswerList) {
            if(!candidateAnswerList.contains(answer)) {
                throw new QuestionExamException("Answer not included in candidate answer list");
            }
        }
        this.trueAnswerList = trueAnswerList;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Question) {
            Question otherQuestion = (Question)other;
            boolean sameQuestion = this.getQuestion().compareTo(otherQuestion.getQuestion())==0;
            boolean sameDescription = this.getDescription().compareTo(otherQuestion.getQuestion())==0;
            boolean sameCandidateAnswerList = true;
            boolean sameTrueAnswerList = true;

            boolean equals = sameQuestion && sameDescription && sameCandidateAnswerList && sameTrueAnswerList;
			return equals;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuestion()+getDescription());
    }

    @Override
    public String toString() {
        return question;
    }
}
