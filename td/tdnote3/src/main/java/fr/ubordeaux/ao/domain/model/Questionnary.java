package fr.ubordeaux.ao.domain.model;

import fr.ubordeaux.ao.domain.exception.QuestionExamException;

import java.util.*;

public class Questionnary {
    private String id;
    private List<Question> questionList;
    private Map<Question,String> answerMap;

    public Questionnary(String id, List<Question> questionList) {
        this.listId(id);
        this.listQuestionList(questionList);
        answerMap = new HashMap<Question,String>();
    }

    public String getId() {
        return id;
    }

    public List<Question> getQuestionList() {
        return this.questionList;
    }

    public int getScore() {
        int score = 0;
        Iterator i = answerMap.keySet().iterator();
        Question q = null;
        String answer = null;
        while (i.hasNext()) {
            q = (Question)i.next();
            answer = (String)answerMap.get(q);
            if(q.getTrueAnswerList().contains(answer)) {
                score++;
            }
            System.out.println(answer);
        }
        return score;
    }

    public void answer(Question question, String answer) {
        this.answerMap.put(question, answer);
    }


    private void listId(String id) {
        if (id == null) throw new QuestionExamException("cannot create questionnary with null id");
        this.id = id;
    }

    private void listQuestionList(List<Question> questionList) {
        if (questionList == null) throw new QuestionExamException("cannot create questionnay with null question");
        this.questionList = questionList;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Questionnary) {
            Questionnary otherQuestionnay = (Questionnary) other;
            boolean sameId = this.getId().compareTo(otherQuestionnay.getId())==0;
            
            boolean equals = sameId;
			return equals;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return getId();
    }
}
