package fr.ubordeaux.ao.domain.model;

import java.util.Set;

public interface Catalog {
    int size();
    String name();
    public Set<Reference> getAllReferences();
    public Set<Reference> getOwnReferences();
    Reference findReferenceById(String id);
    void addReference(Reference reference);
    public void addSubCatalog(Catalog catalog);
    void removeReference(Reference reference);
    public void removeSubCatalog(Catalog catalog);
}
