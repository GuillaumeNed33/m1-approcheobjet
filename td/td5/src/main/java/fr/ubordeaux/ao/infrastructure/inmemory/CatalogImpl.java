package fr.ubordeaux.ao.infrastructure.inmemory;

import fr.ubordeaux.ao.domain.exception.ReferenceManagementException;
import fr.ubordeaux.ao.domain.model.Catalog;
import fr.ubordeaux.ao.domain.model.Reference;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CatalogImpl implements Catalog {
    private Map<String, Reference> references;
    private String name;
    private Set<Catalog> subCatalogs;

    public CatalogImpl() {
        this.subCatalogs = new HashSet<>();
        this.references = new HashMap<String, Reference>();
        this.name = "";
    }

    public void setName(String name) {
        if(name.equals("") || name.length() < 3 || name.length() > 10)
            throw new Error("Le nom ne correspond pas. Requis : minuscules entre 3 et 10 caractères");
        this.name = name.toLowerCase();
    }

    public CatalogImpl(String name) {
        this.subCatalogs = new HashSet<>();
        this.references = new HashMap<String, Reference>();
        this.name = name.toLowerCase();
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int size() {
        return this.references.size();
    }

    @Override
    public Set<Reference> getAllReferences() {
        if (subCatalogs.isEmpty())
            return getOwnReferences();
        Set<Reference> recursiveReferencesSet = getOwnReferences();
        for (Catalog c : subCatalogs) {
            recursiveReferencesSet.addAll(c.getAllReferences());
        }
        return recursiveReferencesSet;
    }

    @Override
    public Set<Reference> getOwnReferences() {
        return new HashSet<Reference>(references.values());
    }

    @Override
    public Reference findReferenceById(String id) {
        if (!this.references.containsKey(id)) {
            throw new ReferenceManagementException(
                    "cannot find Reference, id unknown !"
            );
        }
        return references.get(id);
    }

    @Override
    public void addReference(Reference reference) {
        this.references.put(reference.getId(), reference);
    }

    @Override
    public void removeReference(Reference reference) {
        references.remove(reference.getId());
    }

    @Override
    public void addSubCatalog(Catalog catalog) {
        for (Catalog c : subCatalogs) {
            if (c.name().equals(catalog.name()))
                throw new ReferenceManagementException("Invalid catalog name");
        }
        subCatalogs.add(catalog);
    }

    @Override
    public void removeSubCatalog(Catalog catalog) {
        boolean ok = false;
        for (Catalog c : subCatalogs) {
            if (c.name().equals(catalog.name()) && c.size() == catalog.size()) {
                ok = true;
                subCatalogs.remove(catalog);
            }
        }
        if(!ok)
            throw new ReferenceManagementException("Invalid catalog name");
    }


}
