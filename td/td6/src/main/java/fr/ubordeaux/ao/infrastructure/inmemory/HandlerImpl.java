package fr.ubordeaux.ao.infrastructure.inmemory;

import fr.ubordeaux.ao.application.Command;
import fr.ubordeaux.ao.application.Handler;
import fr.ubordeaux.ao.domain.exception.ReferenceManagementException;
import fr.ubordeaux.ao.domain.model.Catalog;
import fr.ubordeaux.ao.domain.model.KeyWordMap;

public class HandlerImpl implements Handler {

	private Catalog rootCatalog;
	private KeyWordMap keywordMap;


	public HandlerImpl(Catalog rootCatalog, KeyWordMap keywordMap) {
	    this.setRootCatalog(rootCatalog);
		this.setKeyWordMap(keywordMap);
	}

	private void setRootCatalog(Catalog rootCatalog) {
	    if (rootCatalog == null) throw new ReferenceManagementException("cannot create HandlerImpl with null as root catalog");
	    this.rootCatalog = rootCatalog;
	}
	
	private void setKeyWordMap(KeyWordMap keywordMap) {
	    if (keywordMap == null) throw new ReferenceManagementException("cannot create HandlerImpl with null as a KeyWordMap");
	    this.keywordMap = keywordMap;
	}
    
	@Override
	public void handle(Command command) {
        command.execute(rootCatalog, keywordMap);
	}

}