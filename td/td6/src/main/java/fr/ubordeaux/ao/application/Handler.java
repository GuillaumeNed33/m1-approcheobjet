package fr.ubordeaux.ao.application;

public interface Handler {
    public void handle(Command command);
}