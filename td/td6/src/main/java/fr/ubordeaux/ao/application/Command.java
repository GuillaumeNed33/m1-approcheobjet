package fr.ubordeaux.ao.application;

import fr.ubordeaux.ao.domain.model.Catalog;
import fr.ubordeaux.ao.domain.model.KeyWordMap;

public interface Command {
    public void execute(Catalog rootCatalog, KeyWordMap keywordMap);
}