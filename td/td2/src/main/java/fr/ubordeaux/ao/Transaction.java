package fr.ubordeaux.ao;

import java.util.Date;

public class Transaction {
    private Date date;
    private String title;
    private int amount;

    public Transaction(String title, int amount) {
        //Affecter la date à aujourd'hui
    	if(amount == 0) throw new IllegalArgumentException();
    	this.date = new Date();
        this.title = title;
        this.amount = amount;
    }

	public Date getDate() {
        //TODO_2
    	return (Date)this.date.clone();
    }

    public String getTitle() {
        //TODO_2
    	return this.title;
    }

    public int getAmount() {
        //TODO_2
    	return this.amount;
    }
    
    @Override
	public boolean equals(Object other) {
		if (other instanceof Account) {
			Transaction otherTransaction = (Transaction) other;
			return ( otherTransaction.getDate().compareTo(this.date) == 0 && 
					otherTransaction.getAmount() == this.amount && 
					otherTransaction.getTitle() == this.title );
		}
		return false;
	}
}