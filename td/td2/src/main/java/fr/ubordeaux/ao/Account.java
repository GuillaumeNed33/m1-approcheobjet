package fr.ubordeaux.ao;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class Account {
	private double balance;
	private String id;
	private String name;
	private Set<Transaction> transactions;

	public Account(String name) {
		this.id = UUID.randomUUID().toString();
		this.balance = 0;
		transactions = new HashSet<Transaction>();
	}

	public double getBalance() {
		return balance;
	}

	public void addTransaction(Transaction transaction) {
		//TODO_3
		transactions.add(transaction);
		this.balance += (double)(transaction.getAmount()/100);
	}

	public Set<Transaction> getTransactionSince(Date date) {
		//TODO_1
		Set<Transaction> result = new HashSet<Transaction>();
		for(Transaction t : this.transactions) {
			if(t.getDate().compareTo(date) == 0)
				result.add(t); //Copie de la transaction
		}
		return result;
	}

	public void deleteTransaction(Date date, int amount, String title) {
		for(Transaction t : this.transactions) {
			if(t.getDate().compareTo(date) == 0 && t.getAmount() == amount && t.getTitle() == title)
				this.transactions.remove(t);
		}
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Account) {
			Account otherAccount = (Account) other;
			return this.id.compareTo(otherAccount.id) == 0;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	@Override
	public String toString() {
		return "Account "+this.name+" (id="+this.id+"), balance = "+this.balance;
	}

}