package fr.ubordeaux.ao.domain.model;

import java.util.List;
import java.util.Set;

public interface Catalog {
    void addReference(Reference r);
    void removeReference(Reference r);
    Set<Reference> getReferences();
}