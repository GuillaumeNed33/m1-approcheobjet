package fr.ubordeaux.ao.domain.model;

import fr.ubordeaux.ao.domain.type.Price;

import java.util.Objects;
import java.util.UUID;

public class Reference {
    private String id;
    private String name;
    private String description;
    private Price price;

    public Reference(String id, String name, String description, Price price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reference reference = (Reference) o;
        return id.equals(reference.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Price getPrice() {
        return price;
    }
}