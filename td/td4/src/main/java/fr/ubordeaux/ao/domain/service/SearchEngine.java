package fr.ubordeaux.ao.domain.service;

import fr.ubordeaux.ao.domain.model.Catalog;
import fr.ubordeaux.ao.domain.model.Reference;
import fr.ubordeaux.ao.domain.type.Price;

import java.util.HashSet;
import java.util.Set;

public class SearchEngine {
    //TODO: Ajouter des méthodes permettant de chercher des références dans le catalogue.
    Set<Reference> findAllReferences(Catalog c) {
        return c.getReferences();
    }

    Reference findById(Catalog c, String id) {
        if(id.equals(""))
            throw new Error("Id undefined");
        for (Reference r : c.getReferences()) {
            if(r.getId().equals(id)) {
                return r;
            }
        }
        throw new Error("Reference can't be find with id : " + (id));
    }

    Reference findByName(Catalog c, String name) {
        if(name == null)
            throw new Error("Name undefined");
        for (Reference r : c.getReferences()) {
            if(r.getName().equals(name)) {
                return r;
            }
        }
        throw new Error("Reference can't be find with name : " + name);
    }

    Set<Reference> findByMaxPrice(Catalog c, Price price) {
        Set<Reference> res = new HashSet<>();
        if (price.getPrice() <= 0)
            throw new Error("Price must be positive");
        for (Reference r : c.getReferences()) {
            if (r.getPrice().getPrice() <= price.getPrice()) {
                res.add(r);
            }
        }
        return res;
    }

    Set<Reference> findByMinPrice(Catalog c, Price price) {
        Set<Reference> res = new HashSet<>();
        if (price.getPrice() >= 0)
            throw new Error("Price must be positive");
        for (Reference r : c.getReferences()) {
            if (r.getPrice().getPrice() <= price.getPrice()) {
                res.add(r);
            }
        }
        return res;
    }
}