package fr.ubordeaux.ao.infrastructure.inmemory;

import fr.ubordeaux.ao.domain.model.Catalog;
import fr.ubordeaux.ao.domain.model.Reference;

import java.util.HashSet;
import java.util.Set;

public class CatalogImpl implements Catalog {
    Set<Reference> references;

    public CatalogImpl() {
        this.references = new HashSet<Reference>();
    }

    @Override
    public void addReference(Reference r) {
        if(r == null)
            throw new Error("Reference is undefined");

        if(r.getPrice().getPrice() <= 0)
            throw new Error("Price must be positive");
        if(r.getName().equals(""))
            throw new Error("Name is empty");

        for (Reference ref : this.references) {
            if(ref.getId().equals(r.getId())) {
                throw new Error("Reference already in catalog");
            }
        }
        this.references.add(r);
    }

    @Override
    public void removeReference(Reference r) {
        if(r == null)
            throw new Error("Id undefined");
        boolean found = false;
        for (Reference ref : this.references) {
            if(ref.getId().equals(r.getId())) {
                this.references.remove(r);
                found = true;
            }
        }
        if(!found) {
            throw new Error("Reference can't be find with id : " + r.getId());
        }
    }

    public Set<Reference> getReferences() {
        return this.references;
    }
}