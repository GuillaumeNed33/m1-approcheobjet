package fr.ubordeaux.ao;

import java.util.HashSet;
import java.util.Set;

public class ContactService {
	private Contacts contactList;
	
	public ContactService(Contacts contacts) {
		this.contactList = contacts;
	}
	public Set<Contact> searchContact(/*Contacts contactList,*/String name) {
		Set<Contact> contacts = new HashSet<Contact>();
		for(Contact c : this.contactList.getContacts(0, this.contactList.size()-1)) {
			if(c.getFirstName().contains(name) || c.getSecondName().contains(name)) {
				contacts.add(c);
			}
		}
		return contacts;
	}
}
