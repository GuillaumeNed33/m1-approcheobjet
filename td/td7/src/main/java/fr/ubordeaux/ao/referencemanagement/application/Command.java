package fr.ubordeaux.ao.referencemanagement.application;

import fr.ubordeaux.ao.referencemanagement.domain.exception.ReferenceManagementException;

import java.util.ArrayList;
import java.util.List;

public abstract class Command {

    private String id;
    private List<CommandMonitor> monitors;

    Command(String id) {
        this.id = id;
        monitors = new ArrayList<>();
    }

    Command() {
        id = "untitle";
        monitors = new ArrayList<>();
    }

    public void addMonitor(CommandMonitor cmd) {
        if (cmd == null) {
            throw new ReferenceManagementException("Monitor is null");
        }
        monitors.add(cmd);
    }

    private void notifyMonitor() {
        for(CommandMonitor m : monitors) {
            m.commandIsExecuted(id);
        }
    }

    public void execute() {
        this.executeCommand();
        notifyMonitor();
    }

    abstract void executeCommand();

}